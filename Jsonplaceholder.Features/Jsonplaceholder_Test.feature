Feature: Jsonplaceholder API Automation Test

Scenario: Create a new user to the endpoint - Post Method
		Given User wants to execute CreateNewUser endpoint
		When User submits a POST request to the endpoint
		Then User should get 201 status code and response body message
		
Scenario: Retrieve a single user from the endpoint - Get Method
		Given User wants to execute a GetSingleUser endpoint
		When User submits GET request to the endpoint
		Then User should receive 200 status code and success response message
		
Scenario: Retrieve 10 users from the endpoint - Get Method
		Given User wants to execute GetMultipleUser endpoint
		When User submits a GET request to the endpoint
		Then User should get 200 status code and success response message