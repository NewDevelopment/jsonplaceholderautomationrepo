package Jsonplaceholder_Step_Definitions;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class BDDStyleMethod {
	
	public static Response GetSingleUser() {
		
		Response singleUserResponse = RestAssured.when().get("https://jsonplaceholder.typicode.com/users/1")
				.then().using().extract().response();
		
		return singleUserResponse;
	}
	
	public static Response VerifySingleUserResponse() {
		
		Response resp = BDDStyleMethod.GetSingleUser();
		int statusCode = resp.getStatusCode();
		
		Assert.assertEquals(statusCode, 200);
		
		return resp;
	}
	
	public static Response GetMultipleUsers() {
		
		Response multipleUserResponse = RestAssured.given().get("https://jsonplaceholder.typicode.com/users")
				.then().using().extract().response();
		
		return multipleUserResponse;
	}
	
	
	
	public static Response AddNewUser() {
		String baseUrl = "https://jsonplaceholder.typicode.com/users";
		String JsonBody = "\"{\\r\\n\" + \r\n" + 
				"				\"  \\\"id\\\": \"\",\\r\\n\" + \r\n" + 
				"                                \"  \\\"name\\\": \"Onke\",\\r\\n\" + \r\n" + 
				"				\"  \\\"username\\\": \"Ngcatsha\",\\r\\n\" + \r\n" + 
				"				\"  \\\"email\\\": \"onke@gmail.com\",\\r\\n\" + \r\n" + 
				"				\"  \\\"address\\\": {\\r\\n + \r\n" + 
				"                                \"  \\\"street\\\": \"Joe Gqabi Str\",\\r\\n\" +\r\n" + 
				"                                \"  \\\"suite\\\": \"APT. 114\",\\r\\n\" +\r\n" + 
				"                                \"  \\\"city\\\": \"Joe Gqabi Str\",\\r\\n\" +\r\n" + 
				"                                \"  \\\"zipcode\\\": \"7785\",\\r\\n\" +\r\n" + 
				"                                \"  \\\"geo\\\":{\\r\\n\" +\r\n" + 
				"                                \"  \\\"lat\\\": \"-56.2545\",\\r\\n\" +\r\n" + 
				"                                \"  \\\"lng\\\": \"85.5847\",\\r\\n\" +\r\n" + 
				"				\"  },\r\n" + 
				"                                \"  }, \\r\\n\" + \r\n" + 
				"				\"  \\\"phone\\\": \\\"0837615798\",\\r\\n\" + \r\n" + 
				"                                \"  \\\"website\\\": \\\"TechSA.com\",\\r\\n\" + \r\n" + 
				"				\"  \\\"company\\\": {\\r\\n\" + \r\n" + 
				"				\"  \\\"name\\\": \\\"TechSA Pty Ltd\",\\r\\n\" + \r\n" + 
				"                                \"  \\\"catchPhrase\\\": \\\"Multi-layered client-server neural-net\",\\r\\n\" + \r\n" + 
				"                                \"  \\\"bs\\\": \\\"harness real-time e-markets\",\\r\\n\" + },\\r\\n\" + \r\n" + 
				"				\"}";
		Response newUserResponse = RestAssured.given().contentType(ContentType.JSON)
				 .body(JsonBody)
				 .when()
				 .post(baseUrl);
				
				return newUserResponse;
	}
	
	public static Response VerifyMultipleUsersResponse() {
		
		Response resp = BDDStyleMethod.GetMultipleUsers();
		int statusCode = resp.getStatusCode();
		
		Assert.assertEquals(statusCode, 200);
		
		return resp;
	}
	
	public static Response VerifyAddNewUsersResponse() {
		
		Response resp = BDDStyleMethod.AddNewUser();
		int statusCode = resp.getStatusCode();
		
		Assert.assertEquals(statusCode, 201);
		
		return resp;
	}

}
