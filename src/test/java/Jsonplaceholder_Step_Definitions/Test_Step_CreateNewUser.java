package Jsonplaceholder_Step_Definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;


public class Test_Step_CreateNewUser {
	
	@Given("^User wants to execute CreateNewUser endpoint$")
	public void user_wants_to_execute_CreateNewUser_endpoint() throws Throwable {
		
		RestAssured.given()
        .contentType(ContentType.JSON)
        .baseUri("https://jsonplaceholder.typicode.com/users");;
        
	}

	@When("^User submits a POST request to the endpoint$")
	public void user_submits_a_POST_request_to_the_endpoint() throws Throwable {
		
		BDDStyleMethod.AddNewUser();
	  
	}

	@Then("^User should get (\\d+) status code and response body message$")
	public void user_should_get_status_code_and_response_body_message(int arg1) throws Throwable {
		
		BDDStyleMethod.VerifyAddNewUsersResponse();
		
	}


}
