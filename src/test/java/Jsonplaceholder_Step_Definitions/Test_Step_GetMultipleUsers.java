package Jsonplaceholder_Step_Definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class Test_Step_GetMultipleUsers {
	
	@Given("^User wants to execute GetMultipleUser endpoint$")
	public void user_wants_to_execute_GetMultipleUser_endpoint() throws Throwable {
		
		RestAssured.given()
		.contentType(ContentType.JSON)
		.baseUri("https://jsonplaceholder.typicode.com/users");
	 
	}

	@When("^User submits a GET request to the endpoint$")
	public void user_submits_a_GET_request_to_the_endpoint() throws Throwable {
		
		BDDStyleMethod.GetMultipleUsers();

	}

	@Then("^User should get (\\d+) status code and success response message$")
	public void user_should_get_status_code_and_success_response_message(int arg1) throws Throwable {
		
		BDDStyleMethod.GetMultipleUsers();
	 
	}

}
