package Jsonplaceholder_Test_Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		
		features = "JsonplaceholderFeatures",
		glue = {"Jsonplaceholder_Step_Definitions"},
		
		plugin = {"pretty", "html:target/cucumber", 
				"json:target/cucumber.json", 
				"com.cucumber.listener.ExtentCucumberFormatter:target/report.html"
				}
		
		)


public class TestRunner {

}
